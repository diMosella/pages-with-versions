import { Props, Flags, Args } from './constants';

export interface PropsAndArgs {
  props: Record<string, string>;
  flags: Flags[];
  args: Args[];
}

export interface Versions {
  instanceVersion: string,
  homeVersion: string,
  otherVersions: string[]
}
